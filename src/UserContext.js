import React from 'react';

//Creates a Context Object

//A context object as the name states is a data type of an object that can be used to store information that cn be shared to other components within the app.
//The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop drilling

const UserContext = React.createContext()

// The "Provider" property of createContext allows other components to consume/use the context object and supply necessary information
export const UserProvider = UserContext.Provider

export default UserContext;