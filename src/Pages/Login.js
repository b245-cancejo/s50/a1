import {Fragment,useContext, useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from "../UserContext.js"
import Swal from 'sweetalert2';

function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	//Button
	const [isActive, setIsActive] = useState(false);

	//login
	//const [user, setUser] = useState(localStorage.getItem("email"));

	//allows us to consume the UserContext object and its properties for user validation

	const {user, setUser} = useContext(UserContext);
	//console.log(user)

	const navigate = useNavigate()


	useEffect(() => {
		if(email !== "" && password !== "" ){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])



	function login(event){
		event.preventDefault();

		//if you want to add the email of the authenticated user in the local storage
			//syntax
			//localStorage.setItem("propertyName", value)

		//Process a fetch request to corresponding backend API
		//Syntax: fetch('url', {options})

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
			.then(result => result.json())
			.then(data => {
				console.log(data)

				if(data === false){
					Swal.fire({
						title: "Error!",
						icon: "Authentication Failed!",
						text: "Please Try Again!"
					})
					//alert('Your credentials are wrong! You can try again or register on our website!')
				}else{
					localStorage.setItem('token', data.auth)
					retrieveUserDetails(localStorage.getItem('token'))

					Swal.fire({
						title: "Authentication Successful!",
						icon: "success",
						text: "Welcome to Course Booking" 
					})
					navigate("/");
				}
			})
		

		/*localStorage.setItem("email", email)
		setUser(localStorage.getItem("email")) 

		alert("Login Successfully!")
		setEmail('');
		setPassword('');
*/
		//navigate("/");
		

	}
	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/user/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => { 
			console.log(data)
			setUser(
					{
						id: data._id,
						isAdmin: data.isAdmin
					}
				)
		})

	}



	return(
			user ?
			<Navigate to = "*"/>
			:
			<Fragment>
				<h1>Login</h1>
				<Form onSubmit = {(event) => login(event)}>
				    <Form.Group className="mb-3" controlId="formBasicEmail">
				      <Form.Label>Email address</Form.Label>
				      <Form.Control 
				      	type="email" 
				      	placeholder="Enter your email" 
				      	value = {email}
				      	onChange = {event => setEmail(event.target.value)}
				      	/>
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicPassword">
				      <Form.Label>Password</Form.Label>
				      <Form.Control 
				      	type="password" 
				      	placeholder="Password" 
				      	value = {password}
				      	onChange = {event => setPassword(event.target.value)}
				      	/>
				    </Form.Group>
				    {
				    	isActive ?
				    	<Button variant="success" type="submit">
				    	  Login
				    	</Button>
				    	:
				    	<Button variant="dark" type="submit" disabled >
				    	  Login
				    	</Button>

				    }
				    
				</Form>
			</Fragment>
		
		)
}

export default Login;