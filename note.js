
// to create a react app,
	//npx create-react-app project-name

//to run out react application
	//npm start

	//files to be removed:
	//from folder src:
		//App.test.js
		//index.css
		//reportWebVitals.js
		//logo.svg

	//we need to delete all the importations of the said files

//the 'import' statement allows us to use the code/export modules from other files similar to how we use the "require" function in NodeJS

//React JS it applies the concepts of rendering and mounting in order to display and create components
//Rendering refers to the process of calling/invoking a component returning set of instruction creating DOM

//Mounting is when REACT JS "renders of displays" the component the initial DOM based on the instruction

//using the event.target.value will capture the value inputted by user on our input form

// The dependencies in useEffect
	//1. Single Dependency
		//on the initial load of the component the side effect/function will triggered and whenever changes occure on our dependency
	//2. empty dependency
		//the sideEffect will only run during the initial load
	//3. multiple dependency [dependency1, dependency2, ...]
		//the sideEffect will run during the initial and whenever the state of the dependencies changed.

//[Section] react-router-dom
	//for us to ve able to use the module/library across all of our pages we have to contain them with BrowserRouter/Router
	//Routes - we contain all of the routes in our react-app
	//Route - specific route wherein we will declare the url and also the component or paege to be mounted

	//we use Link if we want to go from one page to another page
	//we use NavLink if we want to change our page using our Navbar
	
	//The "localStorage.setItem" allows us to manipulate the browser's local storage property to store information indefinitely
	


//[Section] Environment variable
	//environment variables are important in hiding sensitive pieces of information like the backend API which can be exploited if added directly into our code.