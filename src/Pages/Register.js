import{Button, Form} from 'react-bootstrap';
import {Fragment} from 'react'

//import the hooks that are needed in our page
import {useState, useContext, useEffect} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2';


function Register(){

	//Create 3 new states where we will store the value from input of email, password and confirmPassword

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [mobileNo, setmobileNo] = useState("")

	//create another state for the button
	const[isActive, setIsActive] = useState(false)

	//const [user, setUser] = useState(localStorage.getItem("email"))

	
	const {user, setUser} = useContext(UserContext)
	console.log(user)
	

	const navigate = useNavigate();

	// useEffect(() => {
	// 	console.log(email)
	// 	console.log(password)
	// 	console.log(confirmPassword)
	// }, [email, password, ])

	useEffect(() => {
		if(email !== "" && password !== "" && confirmPassword!== "" && password === confirmPassword){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password, confirmPassword])

	function register(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'				
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				mobileNo: mobileNo
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Account Created!",
					icon: "success",
					text: " Welcome to our Website!"
				})

				navigate("/login")
			}
		})

		/*localStorage.setItem("email", email)
		setUser(localStorage.getItem("email"))

		alert("Congratulation, you are now registered on our website!")
		setEmail('');
		setPassword('');
		setConfirmPassword('');

		
		navigate("/");*/

	}

	
	return(
		user ?
		<Navigate to = "*" />
		:
		<Fragment>
			<h1 className = "text-center mt-5">Register</h1>
			<Form className = "mt-5" onSubmit = {event => register(event)}>

			      <Form.Group className="mb-3" controlId="formBasicFirstName">
			        <Form.Label>First Name</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Enter your First Name" 
				        value = {firstName}
				        onChange = {event => setFirstName(event.target.value)}
				        required	
				        	/>
			      	</Form.Group>

			      	 <Form.Group className="mb-3" controlId="formBasicLastName">
			        <Form.Label>Last Name</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Enter your Last Name" 
				        value = {lastName}
				        onChange = {event => setLastName(event.target.value)}
				        required	
				        	/>
			      	</Form.Group>

			      	 <Form.Group className="mb-3" controlId="formBasicMobileNumber">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Enter your Mobile No." 
				        value = {mobileNo}
				        onChange = {event => setmobileNo(event.target.value)}
				        required	
				        	/>
			      	</Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter email" 
				        value = {email}
				        onChange = {event => setEmail(event.target.value)}
				        required	
				        	/>
			        <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>
			      	</Form.Group>

			      	<Form.Group 
				      className="mb-3" 
				      controlId="formBasicPassword">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password" 
			        	value = {password}
			        	onChange = {event => setPassword(event.target.value)}
			        	required	
			        	/>
			      </Form.Group>

			       <Form.Group 
			       		className="mb-3" 
			       		controlId="formBasicConfirmPassword">
			        <Form.Label>Confirm Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Confirm your Password" 
			        	value = {confirmPassword}
			        	onChange = {event => setConfirmPassword(event.target.value)}
			        	required
			        	/>
			      </Form.Group>
			    
			    {/*in this code block we do conditional rendering depending on the state of our isActive*/}
			      {
			      	isActive? 
			      	<Button variant="primary" type="submit" >
			        	Submit
			      	</Button>
			      	:
			      	<Button variant="danger" type="submit" disabled>
			      	  	Submit
			      	</Button>

			      }

			     
			   </Form>
		</Fragment>
		)
}

export default Register;