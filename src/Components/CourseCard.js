import {Card, Col, Row, Button} from 'react-bootstrap';
import {useState, useContext, useEffect} from 'react';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';



function CourseCard({courseProp}){

	
	const {user} = useContext(UserContext)
	//The "course" in the CourseCard component is called a "prop" which is a shorthand for property

	//the curly braces are used for props to signify that we are providing information using expressions
	const { _id, name, description, price } = courseProp

	//use the state hook for this component to be able to store its state.
	//States are used to keep track of information related to individual components
		//const [getter, setter] = useState(initialGetterValue)

	const [enrollees, setEnrollees] = useState(0);
	const[seats, setSeats] = useState(30);

	//add new state that will declare whether the button is disable or not
	const [isDisabled, setIsDisabled] = useState(false)

	//initial value of enrollees state
		//console.log(enrollees);

	//if you want to reassign/change the value of the state
		// setEnrollees(1);
		//console.log(setEnrollees)

	function enroll(){
		
		if(seats > 1 && enrollees < 29){
			setEnrollees(enrollees+1)
			setSeats(seats-1)
		}else{
			alert("Congratulation for making it to the cut")
			setEnrollees(enrollees+1)
			setSeats(seats-1)
		}
	}

	useEffect(()=>{
		if(seats === 0){
			setIsDisabled(true)
		}
	},[seats])

	//Define a 'useEffect' hook to have the 'CourseCard' component do perform a certain task

	//This will run automatically
	//syntax:
		//useEffect(sideEffect/function, [dependencies]);

	//sideEffect/function - it will run on the first load and will reload depending on the dependency array.



	return(
			<Row className = "mt-4">
				
				<Col>
					<Card >
					      
					    <Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PHP {price}</Card.Text>
							<Card.Subtitle>Enrollees:</Card.Subtitle>
							<Card.Text>{enrollees}</Card.Text>
							<Card.Subtitle>Available Seats:</Card.Subtitle>
							<Card.Text>{seats}</Card.Text>
							{
								user ?
								<Button as = {Link} to = {`/courses/${_id}`} variant="primary" disabled = {isDisabled}>See More Details</Button>
								:
								<Button as = {Link} to = "/login" variant="primary" >Login</Button>
							}
							
					    </Card.Body>
					      
					</Card>
				</Col>
			</Row>
		)
}

export default CourseCard;