
import './App.css';
import AppNavBar from './Components/AppNavBar.js';
import {Container} from 'react-bootstrap';
// import Banner from './Banner.js';
import {useState, useEffect} from 'react';
// import Highlights from './Highlights.js';
import Home from './Pages/Home.js';
import Courses from './Pages/Courses.js';
import Register from './Pages/Register';
import Login from './Pages/Login.js';
import Logout from './Pages/Logout.js';
import PageNotFound from './Pages/PageNotFound.js'
import CourseView from './Components/CourseView.js'
//import a modules from react-router-dom for the routing

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

//import the UserProvider
import {UserProvider} from './UserContext.js'


function App() {
  
  const [user, setUser] = useState(null)

  useEffect(() => {
    console.log(user)
  }, [user])

  const unSetUser = () => {
    localStorage.clear();
    

  }
  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/user/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => { 
      console.log(data)
      if(localStorage.getItem('token') !== null){
        setUser(
            {
              id: data._id,
              isAdmin: data.isAdmin
            }
          )
      }else{
        setUser(null)
      }
      
    })
  }, [])

  //Storing information in a context object is done by providing the information using the correspondng "Provider" and passing information thru the prop value;
  //all information/data provided to the Provider component can be access later on from the object properties.
  return (
      <UserProvider value = {{user, setUser, unSetUser}}>
        <Router>
          <AppNavBar/>
          <Routes>

            <Route path = "/" element = {<Home/>}/>
            <Route path = "/courses" element = {<Courses/>}/>
            <Route path = "/login" element = {<Login/>}/>
            <Route path = "/register" element = {<Register/>}/>
            <Route path = "/logout" element = {<Logout/>}/> 
            <Route path = "/courses/:courseId" element = {<CourseView/>}/> 
            <Route path = '*' element={<PageNotFound/>} />      
          </Routes>
        </Router>
      </UserProvider>    
  );
}

export default App;
