//import BOOTSTRAP CSS
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import {Link, NavLink} from 'react-router-dom';
import {useContext, Fragment} from 'react';

import UserContext from '../UserContext';


export default function AppNavBar(){
	//if you want to get the item in our localStorage you can use the .getItem("propertyName")
	//console.log(localStorage.getItem("email"))

	//let us create a new state for the user
	//const [user, setUser] = useState(localStorage.getItem("email"))
	const {user} = useContext(UserContext)


	return(
			<Navbar bg="light" expand="lg">
			      <Container>
			      {/*using the "as" keyword, Navbar.Brand inherites the properties of the Link*/}
			      {/*using the "to" keyword Links the Navbar.Brand to the declared url endpoint*/}
			        <Navbar.Brand as = {Link} to = "/">Zuitt</Navbar.Brand>
			        <Navbar.Toggle aria-controls="basic-navbar-nav" />
			        <Navbar.Collapse id="basic-navbar-nav">
			          <Nav className="ms-auto">
			            <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
			            <Nav.Link as = {NavLink} to = "/courses">Courses</Nav.Link>

			            {/*conditional rendering*/}
			            {
			            	user ?
			            	<Nav.Link as = {NavLink} to = "/Logout">Logout</Nav.Link>
			            	:
			            	<Fragment>
				            	<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
				            	<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
			            	</Fragment>

			            }

			            
			            
			          </Nav>
			        </Navbar.Collapse>
			      </Container>
			    </Navbar>
		)
}