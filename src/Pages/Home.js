import {Fragment} from 'react';
import Banner from '../Components/Banner.js';
import Highlights from '../Components/Highlights.js';
import CourseCard from '../Components/CourseCard.js';

export default function Home(){
	return(
		<Fragment>
			<Banner/>
			<Highlights/>
			
		</Fragment>

		)
}