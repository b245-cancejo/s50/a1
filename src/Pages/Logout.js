import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext.js';

function Logout(){

	//localStorage.clear();
	const {unSetUser, setUser} = useContext(UserContext);

	useEffect(() => {
		unSetUser();
		setUser(null);
	},[])
	
	
	return(
			<Navigate to = "/login" />
		)
}

export default Logout;