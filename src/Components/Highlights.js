

import {Container, Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
			
				<Row className = "mt-4">
					{/*First Card*/}
					<Col className = "mx-auto m-md-0 m-1 col-10 col-md-4">
						<Card className ="cardHighlight">
						      
						      <Card.Body>
						        <Card.Title>Learn From Home</Card.Title>
						        <Card.Text>
						          Some quick example text to build on the card title and make up the
						          bulk of the card's content.
						        </Card.Text>
						      
						      </Card.Body>
						</Card>
					</Col>

					{/*Second Card*/}
					<Col className = "mx-auto m-md-0 m-1 col-10 col-md-4">
						<Card className = "cardHighlight">
						      
						      <Card.Body>
						        <Card.Title>Study Now, Pay Later</Card.Title>
						        <Card.Text>
						          Some quick example text to build on the card title and make up the
						          bulk of the card's content.
						        </Card.Text>
						      
						      </Card.Body>
						</Card>
					</Col>
					{/*Third Card*/}
					<Col className = "mx-auto m-md-0 m-1 col-10 col-md-4">
						<Card className	= "cardHighlight">
						      
						      <Card.Body>
						        <Card.Title>Be Part of Our Community</Card.Title>
						        <Card.Text>
						          Some quick example text to build on the card title and make up the
						          bulk of the card's content.
						        </Card.Text>
						      
						      </Card.Body>
						</Card>
					</Col>
				</Row>
			
		)
}