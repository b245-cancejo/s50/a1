import {Fragment} from 'react';
import {Button} from 'react-bootstrap';
import {Link} from "react-router-dom";


function PageNotFound(){
	return(
			<Fragment>
				<h1>Page Not Found</h1>
				<h4>Go back to the <Link to = "/">homepage</Link> </h4> 
			</Fragment>
		)
}

export default PageNotFound;